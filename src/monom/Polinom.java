package monom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Polinom {
	
	public ArrayList<Monom> poli;
	
	
	public Polinom() {
		poli=new ArrayList<Monom>();
	}
	
	
	public int getNrOfMonoms() {
		return poli.size();
	}
	
	
	public Monom getMonomByIndex(int i) {
		return poli.get(i);
	}
	
	
	public void addMonom(Monom m) {
		poli.add(m);
	}
	
	
	public Polinom makeFinalVersion(Polinom polinom) {
		Map<Integer, Double>monoame = new HashMap<>();
		for(Monom m : polinom.poli) {
			if(monoame.containsKey(m.getPutere())) {
				monoame.put(m.getPutere(), monoame.get(m.getPutere()) + m.getCoeficient());
			}
			else {
				monoame.put(m.getPutere(), m.getCoeficient());
			}
		}
		Polinom rez = new Polinom();
		for(Map.Entry<Integer, Double>finalMonom : monoame.entrySet()) {
			rez.addMonom(new Monom(finalMonom.getValue(), finalMonom.getKey()));
		}
		return rez;
	}
	
	
	public void sortPolinom() {
		
	Collections.sort(poli, new Comparator<Monom>() {
        @Override
        public int compare(Monom m1, Monom m2)
        {
        	Integer i1 = m1.getPutere();
        	Integer i2 = m2.getPutere();
        	return i1.compareTo(i2);       
        }
    });
	}
	
	
    public boolean equals(Polinom a){
        for(int i=0;i<a.getNrOfMonoms();i++) {
            if (a.getMonomByIndex(i).getCoeficient()!=0) {
                if (!(a.getMonomByIndex(a.getNrOfMonoms() - 1 - i).getCoeficient() == this.getMonomByIndex(i).getCoeficient() && a.getMonomByIndex(a.getNrOfMonoms() - 1 - i).getPutere() == this.getMonomByIndex(i).getPutere())) {
                    return false;
                }
            }
        }
            return true;
    }
	

}

package monom;

public class Monom {
	
	private double coeficient;
	private int putere;
	
			
	public Monom(double coeficient, int putere) {
		this.coeficient = coeficient;
		this.putere = putere;
		
	}
	
	
	public double getCoeficient() {
		return coeficient;
	}
	
	
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	
	
	public int getPutere() {
		return putere;
	}
	
	
	public void setPutere(int putere) {
		this.putere = putere;
	}

	
	@Override
	public String toString() {
		String semn = "";
		if(this.coeficient >= 0) {
			semn = "+";
		}
		else {
			semn = " ";
		}
		if(this.coeficient == 0) {
			return "";
		}
		if(this.putere == 0) {
			return semn + this.coeficient;
		}
		if(this.putere == 1) {
			if (this.coeficient == 1) {
				return semn + "X";
			}
			return semn + this.coeficient + "X";
		}
		if (this.coeficient == 1) {
			return "X^" + this.putere;
		}
		return semn + this.coeficient + "X^" + this.putere;
	}
}

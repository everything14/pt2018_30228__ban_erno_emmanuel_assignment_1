package test;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

class PolinomTestIntegrate extends TestCase  {
    PoliProc modelTest=new PoliProc();
    
    
    @Test
    public void testIntegrate(){                //verificare integrare
        Polinom firstPolInt=new Polinom();      //10x^4+6x^2-5x^1+3x^0
        Polinom resInt=new Polinom();
        Polinom verifInt=new Polinom();         //2x^5+2x^3-2.5x^2+3x^1

        firstPolInt.addMonom(new Monom(10, 4));
        firstPolInt.addMonom(new Monom(6, 2));
        firstPolInt.addMonom(new Monom(-5, 1));
        firstPolInt.addMonom(new Monom(3, 0));

        verifInt.addMonom(new Monom(3.0, 1));
        verifInt.addMonom(new Monom(-2.5, 2));
        verifInt.addMonom(new Monom(2.0, 3));
        verifInt.addMonom(new Monom(2.0, 5));

        resInt=modelTest.integratePolinom(firstPolInt);
        assert(resInt.equals(verifInt));
    }
}
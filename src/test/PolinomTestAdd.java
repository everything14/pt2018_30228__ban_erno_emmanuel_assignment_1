package test;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

class PolinomTestAdd extends TestCase  {
    PoliProc modelTest=new PoliProc();
    
    
    @Test
    public void testAdd(){
    Polinom firstPolAdd=new Polinom();          //3x^2-5x^1+4x^0
    Polinom secondToAdd=new Polinom();          //-x^2+2x^0
    Polinom resAdd=new Polinom();
    Polinom verifAdd=new Polinom();             //2x^2-5x^1+6x^0

    firstPolAdd.addMonom(new Monom(3, 2));
    firstPolAdd.addMonom(new Monom(-5, 1));
    firstPolAdd.addMonom(new Monom(4, 0));
    secondToAdd.addMonom(new Monom(-1, 2));
    secondToAdd.addMonom(new Monom(2, 0));
    
    verifAdd.addMonom(new Monom(2.0, 2));
    verifAdd.addMonom(new Monom(-5.0, 1));
    verifAdd.addMonom(new Monom(6.0, 0));

    resAdd=modelTest.addTwoPolinoms(firstPolAdd,secondToAdd);
    assert(resAdd.equals(verifAdd));
    }
}
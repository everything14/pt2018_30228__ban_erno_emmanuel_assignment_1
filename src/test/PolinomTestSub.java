package test;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

class PolinomTestSub extends TestCase  {
    PoliProc modelTest=new PoliProc();
    
    
    @Test
    public void testSub(){                          //test adunare
        Polinom firstPolSub=new Polinom();          //3x^2-5x^1+4x^0
        Polinom secondToSub=new Polinom();          //-1x^2+2x^0
        Polinom resSub=new Polinom();
        Polinom verifSub=new Polinom();             //4x^2-5x^1+2x^0

        firstPolSub.addMonom(new Monom(3, 2));
        firstPolSub.addMonom(new Monom(-5, 1));
        firstPolSub.addMonom(new Monom(4, 0));
        secondToSub.addMonom(new Monom(-1, 2));
        secondToSub.addMonom(new Monom(2, 0));

        verifSub.addMonom(new Monom(4.0, 2));
        verifSub.addMonom(new Monom(-5.0, 1));
        verifSub.addMonom(new Monom(2.0, 0));

        resSub=modelTest.subTwoPolinoms(firstPolSub,secondToSub);      //operatia de adunare
        assert(resSub.equals(verifSub));                //verificarea asertiunii
    }
}
package test;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

class PolinomTestMul extends TestCase  {
    PoliProc modelTest=new PoliProc();
    
    
    @Test
    public void testMul(){                          //verificare operatie de imultire
        Polinom firstPolMul=new Polinom();          //1x^3-5x^1+2x^0
        Polinom secondToMul=new Polinom();          //6x^2+10x^0
        Polinom resMul=new Polinom();
        Polinom verifMul=new Polinom();             //5x^5-20x^3+12x^2-50x^1+20x^0

        firstPolMul.addMonom(new Monom(1, 3));
        firstPolMul.addMonom(new Monom(-5, 1));
        firstPolMul.addMonom(new Monom(2, 0));
        secondToMul.addMonom(new Monom(6, 2));
        secondToMul.addMonom(new Monom(10, 0));
        
        verifMul.addMonom(new Monom(6.0, 5));
        verifMul.addMonom(new Monom(-20.0, 3));
        verifMul.addMonom(new Monom(12.0, 2));
        verifMul.addMonom(new Monom(-50.0, 1));
        verifMul.addMonom(new Monom(20.0, 0));

        resMul=modelTest.mulTwoPolinoms(firstPolMul,secondToMul);
        assert(resMul.equals(verifMul));
    }
}
package test;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

class PolinomTestDerivate extends TestCase  {
    PoliProc modelTest=new PoliProc();
    
    
    @Test
    public void testDerivate(){             //verificare operatia de derivare
        Polinom firstPolDev=new Polinom();      //1x^4-3x^2+7x^1+1x^0
        Polinom resDev=new Polinom();
        Polinom verifDev=new Polinom();         //4x^3-6x^1+7x^0

        firstPolDev.addMonom(new Monom(1, 4));
        firstPolDev.addMonom(new Monom(-3, 2));
        firstPolDev.addMonom(new Monom(7, 1));
        firstPolDev.addMonom(new Monom(1, 0));
        
        verifDev.addMonom(new Monom(7.0, 0));
        verifDev.addMonom(new Monom(-6.0, 1));
        verifDev.addMonom(new Monom(4.0, 3));

        resDev=modelTest.derivatePolinom(firstPolDev);
        assert(resDev.equals(verifDev));
    }
}
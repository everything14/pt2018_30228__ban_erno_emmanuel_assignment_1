package polinomProcessing;

import monom.Monom;
import monom.Polinom;

public class PoliProc {
		
	public Polinom addTwoPolinoms(Polinom poli1, Polinom poli2) {
		Polinom auxiliar = new Polinom();
		Polinom result = new Polinom();
		Polinom whatever = new Polinom();
		for(Monom i : poli1.poli) {
			auxiliar.addMonom(new Monom(i.getCoeficient(), i.getPutere()));
		}
			
		for(Monom j : poli2.poli) {
			auxiliar.addMonom(new Monom(j.getCoeficient(), j.getPutere()));
		}
		result = whatever.makeFinalVersion(auxiliar);
		return result;	
	}
		
	
		public Polinom subTwoPolinoms(Polinom poli1, Polinom poli2) {
			Polinom result = new Polinom();
			Polinom auxiliar = new Polinom();
			Polinom whatever = new Polinom();
			for(Monom i : poli1.poli) {
				auxiliar.addMonom(new Monom(i.getCoeficient(), i.getPutere()));
			}
				
			for(Monom j : poli2.poli) {
				auxiliar.addMonom(new Monom(0 - j.getCoeficient(), j.getPutere()));
			}
			result = whatever.makeFinalVersion(auxiliar);
			return result;
		}
		
		
		public Polinom mulTwoPolinoms(Polinom poli1, Polinom poli2) {
			Polinom result = new Polinom();
			Polinom auxiliar = new Polinom();
			Polinom whatever = new Polinom();
			for(Monom i : poli1.poli) {
				for(Monom j : poli2.poli) {
					auxiliar.addMonom(new Monom(i.getCoeficient() * j.getCoeficient(), i.getPutere() + j.getPutere()));
				}
			}
			
			result = whatever.makeFinalVersion(auxiliar);
			return result;
			
		}
		
		
		public Polinom derivatePolinom(Polinom poli1) {
			Polinom result = new Polinom();
			for(Monom i : poli1.poli) {
				if(i.getPutere() != 0) {
					result.addMonom(new Monom(i.getCoeficient() * i.getPutere(), i.getPutere() - 1));
				}
			}
			return result;
		}
		
		
		public Polinom integratePolinom(Polinom poli1) {
			Polinom result = new Polinom();
			for(Monom i : poli1.poli) {
				result.addMonom(new Monom(i.getCoeficient() / (i.getPutere() + 1), i.getPutere() + 1));
			}
			return result;
		}
		
		
		public Polinom divideTwoPolinoms(Polinom poli1,Polinom poli2) {
			Polinom auxiliar = new Polinom();
			Polinom whatever = new Polinom();
			Polinom result = new Polinom();
			for(Monom i : poli1.poli) {
				for(Monom j : poli2.poli) {
					result.addMonom(new Monom(i.getCoeficient() / j.getCoeficient(), i.getPutere() - j.getPutere()));
				}
			}	
			result = whatever.makeFinalVersion(auxiliar);
			return result;
		}	
	}
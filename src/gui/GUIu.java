package gui;

import monom.Monom;
import monom.Polinom;
import polinomProcessing.PoliProc;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUIu {

	private JFrame frame;
	private JTextField textField_poli1_coef;
	private JTextField textField_poli2_coef;
	private JTextField textField_result;
	private JTextField textField_poli1_putere;
	private JTextField textField_poli2_putere;
	
	PoliProc poliproc=new PoliProc();
	Polinom poli1=new Polinom();
	Polinom poli2=new Polinom();
	private JTextField textField_poli2_result;
	private JTextField textField_RESULT_POLI1;


	public GUIu() {
		
		frame = new JFrame();
		textField_poli1_putere = new JTextField();
		textField_poli2_result = new JTextField();
		textField_RESULT_POLI1 = new JTextField();		
		textField_poli2_putere = new JTextField();
		textField_poli1_coef = new JTextField();
		textField_poli2_coef = new JTextField();
		textField_result = new JTextField();
		
		textField_poli1_coef.setBounds(120, 61, 87, 39);
		textField_poli1_coef.setColumns(10);
		
		JLabel lblPoli = new JLabel("Poli1");
		JLabel lblResult = new JLabel("RESULT");
		lblPoli.setBounds(26, 61, 115, 33);
		JLabel lblPoli_1 = new JLabel("Poli2");
		lblPoli_1.setBounds(26, 214, 115, 33);
		
		
		textField_poli2_coef.setBounds(120, 211, 87, 39);
		textField_poli2_coef.setColumns(10);
		textField_result.setBounds(167, 503, 650, 39);
		textField_result.setColumns(10);
		
		JButton btnSub = new JButton("SUB");
		JButton btnAdd = new JButton("ADD");
		JButton btnMul = new JButton("MUL");
		JButton btnDiv = new JButton("DIV");
		JButton btnIntegrate = new JButton("INTEG");
		JButton btnDeriv = new JButton("DERIV");
		JButton btnSave_POLI1 = new JButton("SAVE");
		JButton btnSave_poli2 = new JButton("SAVE");
		
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res=new Polinom();
				String poli1String = "";
				String poli2String = "";
				String resultString = "";
				res = poliproc.addTwoPolinoms(poli1, poli2);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli2.poli) {
					poli2String = poli2String + i.toString();
					System.out.println(i.toString());
				}
			
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
				textField_poli2_result.setText(poli2String);
			}
		});
		btnAdd.setBounds(433, 57, 171, 41);
		frame.getContentPane().add(btnAdd);
		
		lblResult.setBounds(15, 506, 115, 33);
		frame.getContentPane().add(lblResult);
		
	
		btnSub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res = new Polinom();
				String poli1String = "";
				String poli2String = "";
				String resultString = "";
				res=poliproc.subTwoPolinoms(poli1, poli2);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli2.poli) {
					poli2String = poli2String + i.toString();
					System.out.println(i.toString());
				}
			
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
				textField_poli2_result.setText(poli2String);
			}
		});
		btnSub.setBounds(646, 57, 171, 41);
	
		
		
		btnMul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res = new Polinom();
				String poli1String = "";
				String poli2String = "";
				String resultString = "";
				res = poliproc.mulTwoPolinoms(poli1,poli2);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli2.poli) {
					poli2String = poli2String + i.toString();
					System.out.println(i.toString());
				}
			
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
				textField_poli2_result.setText(poli2String);
			}
		});
		btnMul.setBounds(433, 148, 171, 41);
		
		
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res=new Polinom();
				String poli1String = "";
				String poli2String = "";
				String resultString = "";
				res = poliproc.divideTwoPolinoms(poli1,poli2);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli2.poli) {
					poli2String = poli2String + i.toString();
					System.out.println(i.toString());
				}
			
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
				textField_poli2_result.setText(poli2String);
			}
		});
		btnDiv.setBounds(656, 148, 171, 41);
	
		
	
		btnIntegrate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res = new Polinom();
				String poli1String = "";
				String resultString = "";
				res = poliproc.integratePolinom(poli1);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
			}
		});
		btnIntegrate.setBounds(646, 235, 171, 41);
	
		
	
		btnDeriv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polinom res = new Polinom();
				String poli1String = "";
				String resultString = "";
				res=poliproc.derivatePolinom(poli1);
			
				for(Monom i : res.poli) {
					resultString = resultString + i.toString();
					System.out.println(i.toString());
				}
				for(Monom i : poli1.poli) {
					poli1String = poli1String + i.toString();
					System.out.println(i.toString());
				}
				textField_result.setText(resultString);
				textField_RESULT_POLI1.setText(poli1String);
			}
		});
		btnDeriv.setBounds(433, 235, 171, 41);
	
		
		
		textField_poli1_putere.setBounds(246, 61, 94, 39);
		textField_poli1_putere.setColumns(10);
	
		textField_poli2_putere.setBounds(233, 211, 94, 39);
		textField_poli2_putere.setColumns(10);
		
	
		btnSave_POLI1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Monom m1 = new Monom(Double.parseDouble(textField_poli1_coef.getText()),Integer.parseInt(textField_poli1_putere.getText()));
				poli1.addMonom(m1);
			}
		});
		btnSave_POLI1.setBounds(140, 128, 171, 41);
		

		btnSave_poli2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Monom m1 = new Monom(Double.parseDouble(textField_poli2_coef.getText()),Integer.parseInt(textField_poli2_putere.getText()));
				poli2.addMonom(m1);
				
			}
		});
		btnSave_poli2.setBounds(140, 278, 171, 41);
		
		
	
		textField_poli2_result.setBounds(167, 420, 453, 39);	
		textField_poli2_result.setColumns(10);
		textField_RESULT_POLI1.setBounds(167, 360, 453, 39);
		textField_RESULT_POLI1.setColumns(10);		
		JLabel lblPoli_2 = new JLabel("POLI1");
		lblPoli_2.setBounds(15, 365, 115, 33);		
		JLabel lblPoli_3 = new JLabel("POLI2");
		lblPoli_3.setBounds(15, 422, 115, 33);
		
		
		frame.getContentPane().add(btnSave_POLI1);
		frame.getContentPane().add(lblPoli_3);
		frame.getContentPane().add(textField_poli2_result);
		frame.getContentPane().add(btnSub);
		frame.getContentPane().add(btnSave_poli2);
		frame.getContentPane().add(lblPoli_2);
		frame.getContentPane().add(btnDeriv);
		frame.getContentPane().add(btnIntegrate);
		frame.getContentPane().add(textField_RESULT_POLI1);
		frame.getContentPane().add(btnDiv);
		frame.getContentPane().add(textField_poli1_coef);
		frame.getContentPane().add(textField_result);
		frame.getContentPane().add(textField_poli2_coef);
		frame.getContentPane().add(textField_poli1_putere);
		frame.getContentPane().add(textField_poli2_putere);
		frame.getContentPane().add(btnMul);
		frame.getContentPane().add(lblPoli);
		frame.getContentPane().add(lblPoli_1);

		
		frame.setVisible(true);
		frame.setBounds(100, 100, 921, 658);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	}

	
}
